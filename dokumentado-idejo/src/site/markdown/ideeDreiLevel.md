# Die drei Anwendungs Level

----

Die Idee hinter den drei Anwendungs Level ist, dass sich der Anwender die **"Karten Wolke"** schrittweise erschließen kann.

Auch können die drei Level als langfristig angelegte Roadmap interpretiert werden.

&nbsp; 

## Level 1

#### Schreibe Karten

> im Markdown-Format. 

>&nbsp;

> * **markiere Sie**  (Tags)
* **verlinke Sie** (Web URL's)
* **teile Sie** 
    * Karten werden im Internet sichtbar
    * Unterscheidung zwischen private und öffentliche Karten
    * Berechtigungskonzept (Besitzer,Gruppe,Andere) (Read,Write,Copy,Delete)
* **suche Sie**
* **kommentiere Sie**
* **bewerte Sie** ???

&nbsp;

## Level 2

#### Klassifiziere die Karten

> Einführung eines Klassen-Konzepts (Gruppierung von definierten Attributen)

>&nbsp;

>* **vernetze Sie**
    * Klassifiziere Beziehungen zwischen Karten
* **erforsche/analysiere Sie**
    * Abfrage Sprache (SPARQL/GraphQL)
    * Grafische Darstellung (Netze)
    * Reports erstellen

&nbsp; 

## Level 3

####Erwecke die Karten zum "Leben"

> und programmiere **KartoApps**

>&nbsp;

> * teile Sie
* bewerte Sie
