# Das Klassenmodell

----

![KlassenModel-KartoNubo](../tiddlyWiki/images/KlassenModel-KartoNubo.jpg "")

----

*Entwurf Ende 2015*

&nbsp;

## Meilensteine

> **Schritt 1**
> 
> * Klasse ***Card***
> * Klasse ***Tag***
> * Klasse ***Relation***

&nbsp;

> **Schritt 2**
>
>* Klasse ***Tag*** um Vererbung-Konzept erweitern

&nbsp;

> **Schritt 3**
>
>* Klasse ***Group***
>* Klasse ***Member***

&nbsp;

> **Schritt 4**
>
>* Klasse ***Attribute***

&nbsp;

> **Schritt 5**
> 
>* Klasse ***Class***
>* Klasse ***AttributeDefinition***

&nbsp;

> **Schritt 6**
>
>* Klasse ***Class*** um Vererbung-Konzept erweitern

&nbsp;

> **Schritt 7**
>
>* Klasse ***Person***
>* ***Security*** einführen

&nbsp;

> **Schritt 18**
>
>* ***Versionierung*** einführen