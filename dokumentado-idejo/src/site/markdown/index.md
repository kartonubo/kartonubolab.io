# Willkommen

----

In diesem *Buch* wird versucht die Ideen und Konzepte Rund um das ***KartoNubo-Projekt*** darzustellen.

In den nachfolgenden Kapiteln wird versucht die vielen Ideen und Konzepte nicht nur aufzulisten sondern so zu ordnen, dass ein Anforderungs-Katalog entsteht auf dem eine schrittweise Umsetzung geplant werden kann.

In der kurzen Historie dieses Projekts hat sich gezeigt, dass nur ein einfaches <a href="tiddlyWiki/KartoNuboNotizen.html" target="_blank">Notiz-Wiki</a> nicht als Basis für eine iterative Einmann-Entwicklung ausreicht. 
Auch sind die Anzahl der Ideen mit ihrer unterschiedlichen Schwerpunken, die nach der Erstellung des Notiz-Wikis hinzugekommen, zu umfangreich.

&nbsp;

----

<h2> <div style="color:#ff9900;font-style: italic">Dieses Buch befindet sich gerade im Aufbau! </div></h2>

----

