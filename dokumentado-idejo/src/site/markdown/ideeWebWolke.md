# Web-Wolke

----

![KlassenModel-KartoNubo](../tiddlyWiki/images/KartoNubo-Infrastruktur.jpg "")

----

## Service-Module

> **KartoServo**
>
> Die Implementierung des zentralen Services (REST) über den die Karten verwaltet werden.

&nbsp;

> **KartoCelilo**
>
> Implementierung der Suchmaschine

&nbsp;

> **KartoButiko**  
>
> Dienst zur Verwaltung der KartoNubo-Anwendungen (Aplikoj)

&nbsp;

> **KartoKluzo**
>
> Implementierung des API-Gateway.

&nbsp;

> **KartoBroker**
>
> Vermittlungs-Service über denen alle "weltweit" verteilten KartoNubo-Dienste gefunden werden können.

&nbsp;

## Aplikoj (Anwendungen)

**Hier einige Beispiele:**

* KartoApliko 
* KalendaroApliko (Kalender)
* AdresoApliko (Adressbuch)
* PrezentoApliko (Präsentation)
* ...


