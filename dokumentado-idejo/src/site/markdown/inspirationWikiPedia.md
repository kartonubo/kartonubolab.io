# WikiPedia

----

Als frei Enzyklopädie in vielen Sprachen ist [WikiPedia] eine beliebte Auskunftsquelle.

Dass es in einer Sprache nur genau eine (aktuelle) Version/Variante eines Artikel geben darf, ist eine wesentliche Eigenschaft von WikiPedia.
Dies bedeutet, dass sich mehrere Autoren auf einen gemeinsame Version eines Artikels einigen müssen. 

Für eine Enzyklopädie mag das von Vorteil sein, führt aber immer mal wieder zu Meinungsverschiedenheiten. Auch bedeutet dies, das mehrere auch strittige Ansichten nicht neben einander existieren können.

Diese Einschränkung soll es bei KartoNubo nicht geben. Hier können mehrere gleichnamige Karten (Artikel) nebeneinander existieren. Dies führt natürlich zu ganz anderen Herausforderungen (Bewertungsstrategie, Fake, ...).

Die Existenz mehrerer Karten wird durch eine zusätzliche ***GruppenId*** ermöglicht. Weiter Informationen sind im Artikel [Inspiration Maven](inspirationMaven.html) zu finden.

[WikiPedia]: https://de.wikipedia.org/wiki/Wikipedia