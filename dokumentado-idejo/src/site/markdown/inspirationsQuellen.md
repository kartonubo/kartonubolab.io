# Inspirations Quellen

----

Ein neues Projekt wie auch ***KartoNubo*** eines ist, entsteht nicht aus dem Nichts.

Dafür gibt es einfach zu viele gute Projekte (Produkte) im &nbsp; *WWW* &nbsp; die sich mit ihren Ideen und Konzepten als Inspirations-Quelle für KartoNubo ausgezeichnet haben.

In den nachfolgenden Unterkapiteln (siehe linkes Menü) werden diese kurz vorgestellt und ihren Beitrag &#x1f60a; zum Projekt dargestellt.