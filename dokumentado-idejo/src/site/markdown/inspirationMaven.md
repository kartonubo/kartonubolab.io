# Maven

----

Zuerst eine Kurzbeschreibe zu [Maven](https://de.wikipedia.org/wiki/Apache_Maven "Link zum Wikipedia Artikel") aus Wikipedia:

Maven ist ein Build-Management-Tool der Apache Software Foundation und basiert auf Java. Mit ihm kann man insbesondere Java-Programme standardisiert erstellen und verwalten.

Die Abhängigkeitsverwaltung in Maven, die zum Bau eines Java-Projeks benötigt werden, liefer die Blaupause für die eindeutige Identifizierung einer Karte innerhalb von KartoNubo.

Hier ein Auszug aus dem Projekt [KartoNuboCore](https://gitlab.com/kartonubo/KartoNuboCore "Gitlab-Link zum Projekt"):

````xml
<project>
   ...
   <dependencies>
       <dependency>
            <groupId>com.orientechnologies</groupId>
            <artifactId>orientdb-server</artifactId>
            <version>2.1.19</version>
       </dependency>
   </dependencies>
   ...
</project>
```` 

Zusätzlich zum Namen der Karte (änderbar), wird eine eindeutige Identität bestehen aus der ***GroupId*** und ***ArtefactId*** erstellt. 

In einer späteren Phase wird im Rahmen einer Versions- bzw. Historien-Verwaltung die ***Version*** hinzu gefügt.