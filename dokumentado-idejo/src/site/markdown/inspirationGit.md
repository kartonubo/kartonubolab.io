# Git (Lab/Hub)

----

[Git] als verteilte Versionsverwaltung von Datei, mit vielen großartigen Eigenschaften, soll auch innerhalb von KartoNubo eingesetzt werden.

Folgende Eigenschaften sollen u.a. genutzt werden können:

* Historienverwaltung
* Tagen
* Forken
* Branching

&nbsp;

Auch Funktionalitäten webbasierten Hosting-Diensten wie z.B. [GitLab] und [GitHub] sollen genutzt werden können um Karten gezielt zu (ver)teilen.

* Verwaltung von Zugriffsberechtigungen
* Anzeige von MarkDown-Datei in einem ansprechenden Html-Layout

&nbsp;

Es macht keinen Sinn diese Funktionalitäten innerhalb von KartoNubo-Modulen nachzubilden.

Daher sollen Import- und Export-Funktion erstellt werden, um Karten in Form von Markdown-Dateien und/oder Json-Datein in Git abzulegen bzw. wieder auszulesen.

Diese Datenein können dann auch von beliebigen einfachen Text-Editoren gelesen und bearbeitet werden und mit Hilfe von Git-Hosting-Dienste weltweit ausgetauscht werden.


[Git]: https://de.wikipedia.org/wiki/Git
[GitLab]: https://de.wikipedia.org/wiki/GitLab
[GitHub]: https://de.wikipedia.org/wiki/GitHub