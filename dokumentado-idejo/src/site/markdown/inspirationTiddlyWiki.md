# TiddlyWiki

----

Hier die Eigenschaften aus [TiddlyWiki] die übernommen werden:

&nbsp;

* **Die Karte ist die kleinste Informations Einheit**

* **Beschreibung in Wiki-Notation** &nbsp; ***(Markdown)***

* **Link zu anderen Karten**

* **Links zu externen Quellen** &nbsp; ***(in neuen Tabulator)***

* **Tagging**

* **Im- und Export von Karten**

&nbsp;

***Hinweis:*** Der Begriff 'Tiddler' wird durch Karte (Karto) ersetzt.

[TiddlyWiki]: http://tiddlywiki.com/languages/de-AT/