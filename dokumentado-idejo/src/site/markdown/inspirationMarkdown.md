# Markdown

----

[Markdown] ist eine vereinfachte Auszeichnungssprache. Sie wurde mit dem Grundgedanken konzipiert, so einfach lesbar und schreibbar wie möglich zu sein.

Auch gibt es viele Tools ([Editoren]), die die Erstellung von Markdown-Dokumenten unterstützen sowie die Konvertierung in unterschiedliche Formaten.

Hier einige Beispiele:

* Html
* Pdf
* Maven-Site
* [GitBook]
* Präsentationen
   * https://remarkjs.com/
   * https://yhatt.github.io/marp/
   * https://github.com/hakimel/reveal.js

&nbsp;

**Auswirkungen auf KartoNubo:**

* Beschreibungstexte der Karten werden im Markdown-Format erstellt.
* Karten sollen zu Dokumenten, Bücher, Präsentationen zusammen gestellt werden können.



[Markdown]: https://de.wikipedia.org/wiki/Markdown
[GitBook]: https://www.gitbook.com/
[Editoren]: http://codeboje.de/markdown-editors/