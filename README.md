# README

----

Maven-Projekt zur Erstellung und Verwaltung der KartoNubo-Dokumentations-Plattform.

Weitere Details befinden sich auf der [Dokumentations-Homepage](https://kartonubo.gitlab.io/index.html) die über GitLab-Pages erstellt wurde.