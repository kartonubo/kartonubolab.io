# JEE-Demo: Domain-Modul

Hier eine kurze Auflistung der wichtigsten Eigenschaften dieses Moduls

## Klassen

* de.barmenia.jeedemo.model.Kunde
* de.barmenia.jeedemo.dao.KundenDAO
* de.barmenia.jeedemo.utilsDaoUtils

## Persistence

* HibernateFramework

* **persistence.xml**
    * nur im Test-Verzeichnis (src/test/resources/META-INF)
    * produktive Variante für Oracle befindet sich im EJB-Modul

## Test

* HSQLDB als "embedded Database"