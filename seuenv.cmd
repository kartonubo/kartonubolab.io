@echo OFF
@REM Setzen der Projektumgebung
@REM
set SEUDIR=d:\Programme\SEU
set PROJECTDIR=%~dp0
echo %PROJECTDIR%

@REM Setzen der Umgebungsvariablen

@REM - für Entwicklungs-Projekt (Java)
set JAVA_HOME=%SEUDIR%\jdk1.8.0_74
set JAVA=%JAVA_HOME%\bin
set M2_HOME=%SEUDIR%\maven-3.3.9
set M2=%M2_HOME%\bin

@REM - für alle Projekte
set PATH=%JAVA%;%M2%;%PATH%

start /D "%PROJECTDIR%" cmd.exe