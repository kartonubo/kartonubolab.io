# Details zum Dokumentations-Projekt

----

## Die Dokumentationssprache

_**ist Deutsch**_, weil der aktuelle Autor nicht fließend Englisch denken und schreiben kann &#x1f60f; .

Das Maven-Site-Plugin, mit dem diese Web-Dokumentation erstellt wird, unterstützt eine mehrsprachige Dokumentationsstruktur.

----

## Entwicklungsumgebung

### Webseiten-Generierung 

* Maven-Site
* und Commandozeile (DOS)

### Editoren

* MdCharm
* Notepad++

### Versionsverwaltung 

* Git
* SourceTree
* GitLab

### BuildServer

* GitLab Pages

----

## Projektstruktur

### Das Haupt-Projekt (Rootmodul)

* Maven-KonfigurationsDatei (pom.xml)
* Webseiten-Konfiguration (site.xml)
* Dokumente (site/markdown)
* Layout und Bilder  (site/resources)

### Die Bücher (Untermodule)

* Maven-KonfigurationsDatei (pom.xml)
* Webseiten-Konfiguration (site.xml)
* Dokumente (site/markdown)
* Layout und Bilder  (site/resources)

----

## Links

**Maven**

* https://maven.apache.org/plugins/maven-site-plugin/

**Markdown**

* https://de.wikipedia.org/wiki/Markdown
* http://markdown.de/
* http://johnmacfarlane.net/babelmark2/faq.html

**Unicode**

* http://www.fileformat.info/info/unicode/block/dingbats/utf8test.htm
* http://character-code.com/emoticons-html-codes.php