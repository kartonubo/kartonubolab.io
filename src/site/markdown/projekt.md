# Das Projekt KartoNubo

----

Der Name _**KartoNubo**_ kommt aus der Sprache [Esperanto] und besteht aus den beiden einzelnen Wörtern  _**Karto (Karte)**_ und _**Nubo (Wolke)**_.

Das Projekt KartoNubo hat das Ziel eine "Kartenwolke" zu entwicklern. Dabei soll es 

* für den kleine privaten Einsatz geeignet sein
* wie auch als Basis für einen großen "Internet-Himmel" bestehend aus vielen kleine und großen KartenWolken (KartoNuboj).

&nbsp;

## Projektstart

Im Mai 2015 wurde das Projekt geboren. Zu diesem Zeitpunkt wurden die ersten Ideen in Form von Modellen, Grafiken und Notizen erstellt. Diese wurden in einem [TiddlyWiki] zusammengefasst und am 15.11.2015 veröffentlicht.  

**Hier der Link zu den ersten  &nbsp; &#x27a5; 
<a href="tiddlyWiki/KartoNuboNotizen.html" target="_blank">KartoNubo-Notizen</a>**

&nbsp;

## Das Core-Modul

ist zur Zeit ein Prototyp-Projekt mit einer experimentellen Implementierung einer Card-Klasse die in einer [OrientDB] persistiert wird.

Weiterführende Informationen befinden sich im Buch [Ideen und Konzepte].


**Hier der Link zum GitLab-Projekt:** 

 &nbsp; &#x27a5; https://gitlab.com/kartonubo/KartoNuboCore

&nbsp;

## Aktueller Status

* Die Weiterentwicklung der Core-Moduls wurde Ende August 2016 angehalten.
* Das Projekt bleibt erst einmal ein Ein-Mann-Projekt.
* Zur Zeit wird dieses Dokumentations-Projekt aufgebaut mit der Intention ein tragfähiges Konzept für das Projekt KartoNubo zu erstellen. Die Entwicklung des Konzepts wird im Buch [Ideen und Konzepte] dokumentiert.




[Esperanto]: http://de.wikipedia.org/wiki/Esperanto
[TiddlyWiki]: http://tiddlywiki.com/languages/de-AT/
[Ideen und Konzepte]: dokumentado-idejo/index.html
[OrientDB]: http://orientdb.com/orientdb/