# Willkommen

----

#### auf der Dokumentations-Plattform des Projekts [KartoNubo]

&nbsp;


----

<h2> <div style="color:#ff9900;font-style: italic">Diese Seite befindet sich gerade im Aufbau! </div></h2>

----

&nbsp;

| <h2>Die Bücher</h2>      | &nbsp; |
|--------------------------|--------|
| **[Ideen und Konzepte]** | ToDo ....|
| **[User Guide]**         | ToDo ... |
| **[Developer Guide]**    | ToDo ... |

&nbsp;

<br /> <br /> <br /> <br /> <br /> <br />

[Ideen und Konzepte]: dokumentado-idejo/index.html
[User Guide]: dokumentado-userGuide/index.html
[Developer Guide]: dokumentado-devGuide/index.html
[KartoNubo]: projekt.html