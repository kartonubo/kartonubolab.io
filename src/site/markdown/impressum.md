# Impressum / Anbieterkennzeichnung

--- 

### Angaben gemäß § 5 TMG:

Anbieter der Internetpräsenzen _**dokumentado.karotonubo.de**_ ist 

Thomas Heynk  <br />
Baroper Str. 243   <br />
44227 Dortmund   <br />

&nbsp;

### Kontakt

E-Mail:  &nbsp; &nbsp; thomas@heynk.de <br />
Mobile:  &nbsp; &nbsp; 01718655684 

&nbsp;

### Verantwortlich für den Inhalt nach § 55 Abs. 2 RStV 

ist Thomas Heynk (Anschrift wie oben).

&nbsp;


### Siehe auch

* [Datenschutzerklärung](datenschutzerklaerung.html)
* [Haftungsausschluss](haftungsausschluss.html)


